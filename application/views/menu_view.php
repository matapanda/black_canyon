
<?php
  include 'template/_header.php';
?>

<style type="text/css">
 * {
  box-sizing: border-box;
}

body {
  background: rgb(19, 19, 19);
  color: #fff;
  font-family: "Noto Sans", sans-serif;
}

.photo-grid {
  height: 100%;
  display: grid;
  gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(340px, 1fr));
  grid-auto-rows: 340px;
}

.card {
  height: 100%;
  width: 100%;
  border-radius: 0px;
  transition: transform 200ms ease-in-out;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
}

.card:hover {
  box-shadow: rgba(2, 8, 20, 0.1) 0px 0.35em 1.175em,
    rgba(2, 8, 20, 0.08) 0px 0.175em 0.5em;
  transform: translateY(-3px) scale(1.1);
}

@media screen and (min-width: 800px) {
  .card-tall {
    grid-row: span 2 / auto;
  }

  .card-wide {
    grid-column: span 2 / auto;
  }
}

</style>


 <!--== Page Title Start ==-->
 <div class="transition-none">
     <section class="title-hero-bg parallax-effect" style="background-image: url(<?= base_url(); ?>assets/template/assets/images/bcsby2.png); padding-top: 10px">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="page-title text-center white-color" >
               <h1>A little piece of heaven.</h1>
               <p>Our delish food and beverages are produced with the finest ingredients, by the best chefs and baristas, and served with pleasure. 
Just sit, be relax, and spoil your tongue with our special menu.</p >
               <div class="breadcrumb mt-20">
                   <!-- Breadcrumb Start -->
                       <ul>
                         <li><a href="<?= base_url(); ?>">Home</a></li>
                         <li>Menu</li>
                       </ul>
                   <!-- Breadcrumb End -->
               </div>
             </div>
           </div>
         </div>

       </div>
     </section>
 </div>
 <!--== Page Title End ==-->


  <!--== Menu List Grid Start ==-->
<section style="padding-top: 15px; padding-bottom: 10px; background: black;">
   <div class="container" style="width: 100%">
      
  <div class="photo-grid">
    <div class="card card-wide card-tall" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
    <div class="card card-tall" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
   
    <div class="card card-wide" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
     <div class="card card-wide" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
    <div class="card card-wide card-tall" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
    <div class="card" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
   
   
   <div class="card card-wide" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
     <div class="card card-wide card-tall" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
    <div class="card card-tall" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
     <div class="card card-wide" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
    <div class="card card-wide card-tall" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
     <div class="card" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
     <div class="card card-wide" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
     <div class="card card-wide" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
     <div class="card" style="background-image:url('<?= base_url(); ?>assets/template/assets/images/three.png')">
    </div>
  </div>
      </div>
</section>
  <!--== Menu List Grid End ==-->


<?php
  include 'template/_footer.php';
?>
