
<?php
  include 'template/_header.php';
?>


 <!--== Page Title Start ==-->
 <div class="transition-none">
     <section class="title-hero-bg parallax-effect" style="background-image: url(<?= base_url(); ?>assets/template/assets/images/bcsby2.png);">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="page-title text-center white-color">
               <h1 class="font-700">Contact Us</h1>
               <div class="breadcrumb mt-20">
                   <!-- Breadcrumb Start -->
                       <ul>
                         <li><a href="<?= base_url(); ?>">Home</a></li>
                         <li>Contach Us</li>
                       </ul>
                   <!-- Breadcrumb End -->
               </div>
             </div>
           </div>
         </div>

       </div>
     </section>
 </div>
 <!--== Page Title End ==-->

	<!-- ABOUT US -->
	<section style="background:url() right no-repeat #fff; padding-bottom:0px;" >
			<div class="container-fluid">
				<div class="row">
				<div class="col-md-6 col-sm-6 pl-90 pr-70">
						<div class="section-title text-left">
							<h1 style="font: bold 4em ; color:black;">Location</h1><br>
							<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1hl3Cbpy-cbsMAzhLrmqcB1-iBrTgG3K-" width="840" height="800" style="padding: left 20px;"></iframe>
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12">
						<img src="<?= base_url(); ?>assets/template/assets/images/surabaya_bc.png" class="img-responsive" style="width: 100%" />
					
				
						</div>
				</div>
	</section>
	<!-- END ABOUT US -->
 


<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>





<script type="text/javascript">
  $('.owl-carousel').owlCarousel({
  autoplay: true,
  autoplayTimeout: 5000,
  autoplayHoverPause: true,
  loop: true,
  margin: 50,
  responsiveClass: true,
  nav: true,
  loop: true,
  stagePadding: 100,
  responsive: {
    0: {
      items: 1
    },
    568: {
      items: 2
    },
    600: {
      items: 3
    },
    1000: {
      items: 3
    }
  }
})
$(document).ready(function() {
  $('.popup-youtube').magnificPopup({
    disableOn: 320,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: true
  });
});
$('.item').magnificPopup({
  delegate: 'a',
});
</script>



<?php
  include 'template/_footer.php';
?>
