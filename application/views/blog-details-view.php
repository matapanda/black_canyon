<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Black Canyon</title>
<link rel="shortcut icon" href="<?= base_url(); ?>assets/template/assets/images/favicon.ico">

<!-- Core Style Sheets -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/template/assets/css/master.css">
<!-- Responsive Style Sheets -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/template/assets/css/responsive.css">
<!-- Revolution Style Sheets -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/settings.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/navigation.css">

</head>
<body>

<!--== Loader Start ==-->
<div id="loader-overlay">
  <div class="loader">
    <img src="<?= base_url(); ?>assets/template/assets/images/loader.svg" width="80" alt="">
  </div>
</div>
<!--== Loader End ==-->

<!--== Wrapper Start ==-->
<div class="wrapper">

  <!--== Header Start ==-->
  <nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav on no-full no-border navbar-scrollspy">
  	<!--== Start Top Search ==-->
    <div class="fullscreen-search-overlay" id="search-overlay"> <a href="<?= base_url(); ?>assets/template/#" class="fullscreen-close" id="fullscreen-close-button"><i class="icofont icofont-close"></i></a>
      <div id="fullscreen-search-wrapper">
        <form method="get" id="fullscreen-searchform">
          <input type="text" value="" placeholder="Type and hit Enter..." id="fullscreen-search-input" class="search-bar-top">
          <i class="fullscreen-search-icon icofont icofont-search">
          <input value="" type="submit">
          </i>
        </form>
      </div>
    </div>
    <!--== End Top Search ==-->
    <div class="container">
      <!--== Start Atribute Navigation ==-->
      <div class="attr-nav hidden-xs sm-display-none">
        <ul>
          <li class="search"><a href="<?= base_url(); ?>assets/template/#" id="search-button"><i class="icofont icofont-search"></i></a></li>
        </ul>
      </div>
      <!--== End Atribute Navigation ==-->

      <!--== Start Header Navigation ==-->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i class="tr-icon ion-android-menu"></i> </button>
        <div class="logo"> <a href="<?= base_url(); ?>assets/template/index.html"> <img class="logo logo-display" src="<?= base_url(); ?>assets/template/assets/images/logo-white.png" alt=""> <img class="logo logo-scrolled" src="<?= base_url(); ?>assets/template/assets/images/logo-black.png" alt=""> </a> </div>
      </div>
      <!--== End Header Navigation ==-->

      <!-- MENU header navbar-->
      <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="nav navbar-nav navbar-center" data-in="fadeIn" data-out="fadeOut">  
          <li><a class="page-scroll" href="<?= base_url(); ?>">Home</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>c_landing_page/about_us">About Us</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>c_landing_page/menu">Menu</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>c_landing_page/blog">Blog</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>c_landing_page/kontak">Contact</a></li>
        </ul>
      </div>
      <!--== /.navbar-collapse ==-->
    </div>

  </nav>
 <!--== Header End ==-->

 <!--== Page Title Start ==-->
 <div class="transition-none">
 <section class="title-hero-bg parallax-effect" style="background-image: url(<?= base_url(); ?>assets/template/assets/images/bcsby2.png);">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="page-title text-center white-color">
               <h1 class="font-700">Blog Details</h1>
               <div class="breadcrumb mt-20">
                   <!-- Breadcrumb Start -->
                       <ul>
                         <li><a href="<?= base_url(); ?>assets/template/index.html">Home</a></li>
                         <li>Blog Details</li>
                       </ul>
                   <!-- Breadcrumb End -->
               </div>
             </div>
           </div>
         </div>

       </div>
     </section>
 </div>
 <!--== Page Title End ==-->

 <!--== Blog Details Start ==-->
  <section class="white-bg">
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12 xs-mb-50">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mb-20 xs-mb-50">
              <div class="post">
                <div class="blog-grid-slider slick">
                  <div class="item"><img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/post/post-02.jpg" alt=""/></div>
                  <div class="item"><img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/post/post-03.jpg" alt=""/></div>
                  <div class="item"><img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/post/post-04.jpg" alt=""/></div>
                </div>
                <div class="post-metas">
                  <div class="post-metas-center">
                    <p class="post-date">April 9, 2019</p>
                  </div>
                </div>
                <div class="post-info all-padding-20">
                   <h3><a href="<?= base_url(); ?>assets/template/home-blog.html">A guide to content management systems</a></h3>
                   <p>The days of weighing up whether or not you need a website for your business are long gone. These days, if you...</p>
                </div>
              </div>
            </div>
            <!--== Post End ==-->

            <div class="col-md-12 col-sm-12 col-xs-12 mb-20">
              <div class="blog-standard">
                <p>We’ve collected 10 Top Tips to to help you deliver better support for your items. Although it’s not mandatory, some of our most successful authors do an awesome job at providing support for their buyers, and we want to share their techniques.</p>
                <blockquote>
                  <p>For some buyers, the difference between items is based on the professionalism of the support offered. So read on to find out how Envato’s top authors deliver fantastic support.</p>
                </blockquote>
              </div>
              <div class="post-tags"> <a href="<?= base_url(); ?>assets/template/#">Design</a> <a href="<?= base_url(); ?>assets/template/#">Branding</a> <a href="<?= base_url(); ?>assets/template/#">Stationery</a> <a href="<?= base_url(); ?>assets/template/#">Development</a> <a href="<?= base_url(); ?>assets/template/#">Concept</a> </div>
              <h2 class="comment-reply-title mt-30">15 Comments</h2>
              <ul class="comment-box">
              <li class="post-comment">
                <div class="comment-content"> <a href="<?= base_url(); ?>assets/template/#" class="avatar"><img src="<?= base_url(); ?>assets/template/assets/images/team/avatar-1.jpg" alt="#"></a>
                  <div class="post-body">
                    <div class="comment-header"> <span class="author"><a href="<?= base_url(); ?>assets/template/#">Karl Casey</a></span> <span class="time-ago"><a href="<?= base_url(); ?>assets/template/#">25 mins ago</a></span> </div>
                    <div class="post-message">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quam justo, ullamcorper tincidunt pellentesque in, condimentum ut enim.</p>
                    </div>
                    <div class="comment-footer"> <span class="reply"><a href="<?= base_url(); ?>assets/template/#"><i class="ion-reply"></i> Reply</a></span> </div>
                  </div>
                </div>
                <ul class="reply-comment">
                  <li class="post-comment">
                    <div class="comment-content"> <a href="<?= base_url(); ?>assets/template/#" class="avatar"><img src="<?= base_url(); ?>assets/template/assets/images/team/avatar-2.jpg" alt="#"></a>
                      <div class="post-body">
                        <div class="comment-header"> <span class="author"><a href="<?= base_url(); ?>assets/template/#">Lynda Stone</a></span> <span class="time-ago"><a href="<?= base_url(); ?>assets/template/#">18 days ago</a></span> </div>
                        <div class="post-message">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                        <div class="comment-footer"> <span class="reply"><a href="<?= base_url(); ?>assets/template/#"><i class="ion-reply"></i> Reply</a></span> </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
              <li class="post-comment">
                <div class="comment-content"> <a href="<?= base_url(); ?>assets/template/#" class="avatar"><img src="<?= base_url(); ?>assets/template/assets/images/team/avatar-3.jpg" alt="#"></a>
                  <div class="post-body">
                    <div class="comment-header"> <span class="author"><a href="<?= base_url(); ?>assets/template/#">Roland Buford</a></span> <span class="time-ago"><a href="<?= base_url(); ?>assets/template/#">3 months ago</a></span> </div>
                    <div class="post-message">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quam justo, ullamcorper tincidunt pellentesque in, condimentum ut enim.</p>
                    </div>
                    <div class="comment-footer"> <span class="reply"><a href="<?= base_url(); ?>assets/template/#"><i class="ion-reply"></i> Reply</a></span> </div>
                  </div>
                </div>
              </li>
            </ul>

            <div id="respond" class="comment-respond">
              <h2 id="reply-title" class="comment-reply-title">Leave a Comment</h2>
              <form method="post" id="form-comments" class="comment-form contact-form-style-01">
                <div class="row-form row">
                  <div class="col-form col-md-6">
                    <div class="form-group">
                      <input type="text" name="name" class="md-input" id="name" required="" placeholder="Name *" data-error="Your Name is Required">
                    </div>
                  </div>
                  <div class="col-form col-md-6">
                    <div class="form-group">
                      <input type="email" name="email" class="md-input" id="email" placeholder="Email*" required="" data-error="Please Enter Valid Email">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <textarea name="message" class="md-textarea" id="message" rows="7" placeholder="Your Comments" required="" data-error="Please, Leave us a message"></textarea>
                </div>

                <p class="form-submit">
                  <button class="btn btn-color btn-md btn-default remove-margin">Post Comments</button>
                  <input type="hidden" name="comment_post_ID">
                  <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                </p>
              </form>
            </div>

            <h2 class="recent-post-title">Recent Posts</h2>
            <div class="row blog-style-01">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="post">
                  <div class="post-img">
                    <img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/post/post-01.jpg" alt=""/>
                    <div class="post-metas">
                      <div class="post-metas-center">
                        <p class="post-date">January 15, 2018</p>
                      </div>
                    </div>
                  </div>
                    <div class="post-info all-padding-20">
                      <h3><a href="<?= base_url(); ?>assets/template/home-blog.html">Creating a website for your business</a></h3>
                      <p>If you’re looking to create a website for your business, then you’ve probably come across the term content management system, or CMS...</p>
                    </div>
                </div>
              </div>
              <!--== Post End ==-->

              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="post">
                  <div class="blog-grid-slider slick">
                    <img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/post/post-02.jpg" alt=""/>
                    <img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/post/post-08.jpg" alt=""/>
                    <img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/post/post-09.jpg" alt=""/>
                  </div>
                  <div class="post-metas">
                    <div class="post-metas-center">
                      <p class="post-date">September 28, 2017</p>
                    </div>
                  </div>
                    <div class="post-info all-padding-20">
                      <h3><a href="<?= base_url(); ?>assets/template/home-blog.html">A guide to content management systems</a></h3>
                      <p>The days of weighing up whether or not you need a website for your business are long gone. These days, if you...</p>
                    </div>
                </div>
              </div>
              <!--== Post End ==-->
          </div>
          </div>


          </div>
          <div class="row mt-100">
              <div class="col-md-12">
                  <div class="text-center">
                    <div class="pagination dark-color">
                          <ul>
                              <li><a href="<?= base_url(); ?>assets/template/#"><i class="icofont icofont-long-arrow-left mr-5 xs-display-none"></i> Prev</a></li>
                              <li class="active"><a href="<?= base_url(); ?>assets/template/#">1</a></li>
                              <li><a href="<?= base_url(); ?>assets/template/#">2</a></li>
                              <li><a href="<?= base_url(); ?>assets/template/#">3</a></li>
                              <li><a href="<?= base_url(); ?>assets/template/#">Next <i class="icofont icofont-long-arrow-right ml-5 xs-display-none"></i></a></li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12">
          <div class="widget sidebar_widget">
            <h5 class="aside-title">Newsletter</h5>
            <form class="search-form" method="get">
              <input type="text" name="name" class="md-input" id="search" required="" placeholder="Type what it's your mind...">
              <button type="submit" class="icofont icofont-search-alt-2 search-submit"></button>
            </form>
          </div>
          <div class="mb-50">
            <h5 class="aside-title">About Me</h5>
            <a href="<?= base_url(); ?>assets/template/about-me.html">
              <img src="<?= base_url(); ?>assets/template/assets/images/about-me.jpg" alt="about-me" />
            </a>
            <p class="mt-30">Lorem ipsum dolor sit amet consectetur adipiscing elit. Sed dui lorem adipiscing in adipiscing et.</p>
            <a class="btn btn-md btn-dark-outline btn-square mt-10">About Author</a>
          </div>
          <div class="sidebar_widget widget_archive mb-50">
            <h5 class="aside-title">Archive</h5>
            <ul>
              <li> <a href="<?= base_url(); ?>assets/template/#">January 2018</a> <span>14</span></li>
              <li> <a href="<?= base_url(); ?>assets/template/#">May 2017</a> <span>8</span></li>
              <li> <a href="<?= base_url(); ?>assets/template/#">June 2017</a> <span>63</span></li>
              <li> <a href="<?= base_url(); ?>assets/template/#">Febuary 2012</a> <span>45</span></li>
              <li> <a href="<?= base_url(); ?>assets/template/#">April 2016</a> <span>88</span></li>
            </ul>
          </div>
          <div class="social-icons-style-06 mb-50">
            <h5 class="aside-title">Follow Us</h5>
            <ul class="xs-icon">
              <li><a class="icon facebook" href="<?= base_url(); ?>assets/template/#."><i class="icofont icofont-social-facebook"></i></a></li>
              <li><a class="icon twitter" href="<?= base_url(); ?>assets/template/#."><i class="icofont icofont-social-twitter"></i></a></li>
              <li><a class="icon tumblr" href="<?= base_url(); ?>assets/template/#."><i class="icofont icofont-social-tumblr"></i></a></li>
              <li><a class="icon flicker" href="<?= base_url(); ?>assets/template/#."><i class="icofont icofont-social-flikr"></i></a></li>
              <li><a class="icon instagram" href="<?= base_url(); ?>assets/template/#."><i class="icofont icofont-social-instagram"></i></a></li>
            </ul>
          </div>

          <div class="sidebar_widget widget_categories mb-50">
            <h5 class="aside-title">Categories</h5>
            <ul>
              <li> <a href="<?= base_url(); ?>assets/template/#">Business</a></li>
              <li> <a href="<?= base_url(); ?>assets/template/#">Health</a> </li>
              <li> <a href="<?= base_url(); ?>assets/template/#">Motion Graphic</a> </li>
              <li> <a href="<?= base_url(); ?>assets/template/#">Conecpt Design</a> </li>
              <li> <a href="<?= base_url(); ?>assets/template/#">Lifestyle</a> </li>
            </ul>
          </div>
          <div class="sidebar_widget widget_tag_cloud mb-50">
            <h5 class="aside-title">Tags</h5>
            <div class="post-tags"> <a href="<?= base_url(); ?>assets/template/#">Branding</a> <a href="<?= base_url(); ?>assets/template/#">Marketing</a> <a href="<?= base_url(); ?>assets/template/#">Photography</a> <a href="<?= base_url(); ?>assets/template/#">Illustration</a> <a href="<?= base_url(); ?>assets/template/#">Development</a> <a href="<?= base_url(); ?>assets/template/#">Multipurpose</a> <a href="<?= base_url(); ?>assets/template/#">Creativity</a> <a href="<?= base_url(); ?>assets/template/#">Apps</a>  <a href="<?= base_url(); ?>assets/template/#">Fashion</a> <a href="<?= base_url(); ?>assets/template/#">Concept</a> <a href="<?= base_url(); ?>assets/template/#">Design</a></div>
          </div>
        </div>
      </div>
    </div>

  </section>
  <!--== Blog Details End ==-->

 <!--== Footer Start ==-->
  <footer class="footer">
    <div class="footer-main">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-4">
            <div class="widget widget-text">
              <div class="logo-footer">
                <a href="<?= base_url(); ?>assets/template/index.html"><img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/logo-footer.png" alt=""></a>
              </div>
              <p><i class="icofont icofont-location-pin"></i> Jl. Ahmad Yani, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur</p>
              <p><i class="icofont icofont-phone"></i> 031-99860502</p>
              <p class="mt-20 mb-0"><i class="icofont icofont-email"></i><a href="<?= base_url(); ?>assets/template/#"> indonesiablackcanyon@gmail.com</a></p>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="widget widget-text widget-links">
              <h5 class="widget-title">Location</h5>
              <p>
                  <a href="https://goo.gl/maps/GBBUPBBb7Sq1JtQU7">Surabaya</a><br>
                  <a href="https://goo.gl/maps/P8D2YAqCi8hWKYocA">Makassar</a><br>
                  <a href="https://goo.gl/maps/ijbTSAT83kT9oSjD9">Bali</a><br>      
                  </p>
              
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
              <h5 class="widget-title">Company</h5>
              <ul>
                <li><a href="#">Our Menu</a></li>
                <li><a href="#">Location</a></li>
                <li><a href="#">Contach Us</a></li>
                <li><a href="#">Connect your worlds.</a></li>
              
              </ul>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
              <h5 class="widget-title">Media Sosial</h5>
            <ul class="sm-icon">
                <li><a class="facebook" href="#"><i class="icofont icofont-social-facebook"></i><span></span></a></li>
                <li><a class="twitter" href="#"><i class="icofont icofont-social-twitter"></i><span></span></a></li>
                <li><a class="behance" href="https://www.instagram.com/blackcanyonindonesia//"><i class="icofont icofont-social-instagram"></i><span></span></a></li>
              
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="copy-right text-center">© 2021 Black Canyon Indonesia. All rights reserved</div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--== Footer End ==-->

  <!--== Go to Top  ==-->
  <a href="<?= base_url(); ?>assets/template/javascript:" id="return-to-top"><i class="icofont icofont-arrow-up"></i></a>
  <!--== Go to Top End ==-->

</div>
<!--== Wrapper End ==-->

<!--== Javascript Plugins ==-->
<script src="<?= base_url(); ?>assets/template/https://maps.googleapis.com/maps/api/js?key=AIzaSyDJNGOwO2hJpJ9kz8e0UUPjZhEbgDJTTXE"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/plugins.js"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/master.js"></script>

<!-- Revolution js Files -->
<script src="<?= base_url(); ?>assets/template/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.actions.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.carousel.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.kenburn.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.layeranimation.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.migration.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.navigation.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.parallax.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.slideanims.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.video.min.js"></script>
<!--== Javascript Plugins End ==-->

</body>
</html>
