<?php
  include 'template/_header.php';
?>
 <!--== Page Title Start ==-->
 <div class="transition-none">
     <section class="title-hero-bg parallax-effect" style="background-image: url(<?= base_url(); ?>assets/template/assets/images/bcsby2.png);">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="page-title text-center white-color">
               <h1 class="font-700">Our Menu</h1>
               <div class="breadcrumb mt-20">
                   <!-- Breadcrumb Start -->
                       <ul>
                         <li><a href="<?= base_url(); ?>">Home</a></li>
                         <li>Menu</li>
                       </ul>
                   <!-- Breadcrumb End -->
               </div>
             </div>
           </div>
         </div>

       </div>
     </section>
 </div>
 <!--== Page Title End ==-->


  <!--== Menu List Grid Start ==-->
  <section id="portfolio" class="white-bg pb-0">
    <div class="container">   
    <iframe src="<?= base_url(); ?>assets/template/assets/images/menu/hotcoffee.pdf" width="100%" height="1200"></iframe>
    </div>

  </section>
  <!--== Menu List Grid End ==-->

 
<?php
  include 'template/_footer.php';
?>