<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>About Us</title>
<link rel="shortcut icon" href="<?= base_url(); ?>assets/template/assets/images/favicon.ico">

<!-- Core Style Sheets -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/template/assets/css/master.css">
<!-- Responsive Style Sheets -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/template/assets/css/responsive.css">
<!-- Revolution Style Sheets -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/settings.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/navigation.css">

</head>
<body>

<!--== Loader Start ==-->
<div id="loader-overlay">
  <div class="loader">
    <img src="<?= base_url(); ?>assets/template/assets/images/loader.svg" width="80" alt="">
  </div>
</div>
<!--== Loader End ==-->

<!--== Wrapper Start ==-->
<div class="wrapper">

  <!--== Header Start ==-->
  <nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav on no-full no-border navbar-scrollspy">
  	<!--== Start Top Search ==-->
    <div class="fullscreen-search-overlay" id="search-overlay"> <a href="<?= base_url(); ?>assets/template/#" class="fullscreen-close" id="fullscreen-close-button"><i class="icofont icofont-close"></i></a>
      <div id="fullscreen-search-wrapper">
        <form method="get" id="fullscreen-searchform">
          <input type="text" value="" placeholder="Type and hit Enter..." id="fullscreen-search-input" class="search-bar-top">
          <i class="fullscreen-search-icon icofont icofont-search">
          <input value="" type="submit">
          </i>
        </form>
      </div>
    </div>
    <!--== End Top Search ==-->
    <div class="container">
      <!--== Start Atribute Navigation ==-->
      <div class="attr-nav hidden-xs sm-display-none">
        <ul>
          <li class="search"><a href="<?= base_url(); ?>assets/template/#" id="search-button"><i class="icofont icofont-search"></i></a></li>
        </ul>
      </div>
      <!--== End Atribute Navigation ==-->

      <!--== Start Header Navigation ==-->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i class="tr-icon ion-android-menu"></i> </button>
        <div class="logo"> <a href="<?= base_url(); ?>assets/template/index.html"> <img class="logo logo-display" src="<?= base_url(); ?>assets/template/assets/images/logo-white.png" alt=""> <img class="logo logo-scrolled" src="<?= base_url(); ?>assets/template/assets/images/logo-black.png" alt=""> </a> </div>
      </div>
      <!--== End Header Navigation ==-->

      <!--== Collect the nav links, forms, and other content for toggling ==-->
      <div class="collapse navbar-collapse" id="navbar-menu">
       <ul class="nav navbar-nav navbar-center" data-in="fadeIn" data-out="fadeOut">  
            <li><a class="page-scroll" href="<?= base_url(); ?>">Home</a></li>
            <li><a class="page-scroll" href="<?= base_url(); ?>about-us">About Us</a></li>
            <li><a class="page-scroll" href="<?= base_url(); ?>menu">Menu</a></li>
            <li><a class="page-scroll" href="<?= base_url(); ?>list-blog">Blog</a></li>
            <li><a class="page-scroll" href="<?= base_url(); ?>contact">Contact</a></li>
        </ul>
      </div>
      <!--== /.navbar-collapse ==-->
    </div>

  </nav>
 <!--== Header End ==-->