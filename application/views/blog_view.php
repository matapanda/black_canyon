<?php
  // $base_url = "http://localhost/adm_bc/";
  $base_url = "http://back-adm.blackcayonindonesia.co.id/";
                        
  $id_article_main = "";
  $title_article = "";
  $category_article = "";
  $tag_article = "";
  $main_img_article = "";
  $content_article = "";

  $create_admin_article = "";
  $create_date_article = "";
  $status_check_article = "";
  $admin_check_article = "";
  $date_check_article = "";
  $status_post_article = "";
  $admin_post_article = "";
  $date_post_article = "";
  $is_delete_article = "";
  $id_admin = "";
  $id_tipe_admin = "";
  $nama_admin = "";

  $arr_bln = $this->time_master->set_indo_month();

  $str_list_card = "";
  if(isset($list_article)){
      if($list_article){
          foreach ($list_article as $key => $value) {

            // print_r("<pre>");
            // print_r($value);
              $id_article_main =  hash("sha256", $value->id_article_main);
              $title_article = $value->title_article;
              $category_article = $value->category_article;
              $tipe_article = $value->tipe_article;
              $tag_article = $value->tag_article;
              $main_img_article = $value->main_img_article;
              // $content_article = $value->content_article;

              $create_admin_article = $value->create_admin_article;
              $create_date_article = $value->create_date_article;
              $status_check_article = $value->status_check_article;
              $admin_check_article = $value->admin_check_article;
              $date_check_article = $value->date_check_article;
              $status_post_article = $value->status_post_article;
              $admin_post_article = $value->admin_post_article;
              $date_post_article = $value->date_post_article;
              $is_delete_article = $value->is_delete_article;
              // $id_admin = $value->id_admin;
              // $id_tipe_admin = $value->id_tipe_admin;
              // $nama_admin = $value->nama_admin;

              $main_img_article = str_replace("base_url/", $base_url, $main_img_article);

              $ex_date = explode(" ", $create_date_article);
                  $th = explode("-", $ex_date[0])[0];
                  $m = explode("-", $ex_date[0])[1];
                  $d = explode("-", $ex_date[0])[2];

              $ex_time = explode(":", $ex_date[1]);
                  $h = $ex_time[0];
                  $mn = $ex_time[1];

              $str_date = $d." ".$arr_bln[(int)$m]." ".$th." ".$h.".".$mn;

              $basic_content = str_replace("base_url/", $base_url, $value->content_article);

              $content_article = str_replace('  ', '', preg_replace('/[\n]+|[\t]+/', '', strip_tags($value->content_article)));
              $str_content_article = $content_article;
              if(strlen($str_content_article) >= 300){
                  $str_content_article = substr($content_article, 0, 300);
              }

              $set_img_first = preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i',$basic_content ,$matches);
              if($matches){
                  $set_img_first = $matches[0]."\"/>";
              }
              // preg_match('/<*img[^>]*src *= *["\']?([^"\']*)/i', $main_img_article, $matches);

              // print_r($set_img_first);

              $link_dt_art = base_url()."page/blog?id_ses=".$id_article_main;

              $str_list_card .= '
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="post">
                  <div class="post-img">
                    '.$set_img_first.'
                    <div class="post-metas">
                      <div class="post-metas-center">
                        <p class="post-date">'.$str_date.'</p>
                      </div>
                    </div>
                  </div>
                    <div class="post-info all-padding-20">
                      <h3><a href="'.$link_dt_art.'">'.$title_article.'</a></h3>
                      <p>'.$str_content_article.' <a href="'.$link_dt_art.'">.&nbsp;.&nbsp;.&nbsp;Read More</a></p>
                    </div>
                </div>
              </div>';
          }
      }
  }
?>

<?php
  include 'template/_header.php';
?>

 <!--== Page Title Start ==-->
<div class="transition-none">
  <section class="title-hero-bg parallax-effect" style="background-image: url(<?= base_url(); ?>assets/template/assets/images/bcsby2.png);">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="page-title text-center white-color">
              <h1 class="font-700">Blog Posts</h1>
              <div class="breadcrumb mt-20">
                  <!-- Breadcrumb Start -->
                      <ul>
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Blog Posts</li>
                      </ul>
                  <!-- Breadcrumb End -->
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
</div>
 <!--== Page Title End ==-->

 <!--== Blog Start ==-->
<section class="white-bg">
  <div class="container">
    <div class="row blog-style-01">
      
      <?=$str_list_card?>
      <!--== Post End ==-->
    </div>
    
    <div class="row" style="margin-top:50px;">
        <div class="col-12 text-right">
            <?php
                print_r($output);
            ?>
        </div>
    </div>
  </div>
</section>
 <!--== Blog End ==-->


<script>
  function set_link(url){
      window.location.href = url;
  }
</script>



<?php
  include 'template/_footer.php';
?>
