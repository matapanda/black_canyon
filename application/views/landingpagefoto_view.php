<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Black Canyon Cafe</title>
<link rel="shortcut icon" href="<?= base_url(); ?>assets/template/assets/images/favicon.ico">

<!-- Core Style Sheets -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/template/assets/css/master.css">
<!-- Responsive Style Sheets -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/template/assets/css/responsive.css">
<!-- Revolution Style Sheets -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/settings.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/navigation.css">

</head>
<body>

<!--== Loader Start ==-->
<div id="loader-overlay">
  <div class="loader">
    <img src="<?= base_url(); ?>assets/template/assets/images/image3324.png" width="1000" alt="">
  </div>
</div>
<!--== Loader End ==-->

<!--== Wrapper Start ==-->
<div class="wrapper">

  <!--== Header Start ==-->
  <nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav on no-full no-border">
  	<!--== Start Top Search ==-->
    <div class="fullscreen-search-overlay" id="search-overlay"> <a href="<?= base_url(); ?>assets/template/#" class="fullscreen-close" id="fullscreen-close-button"><i class="icofont icofont-close"></i></a>
      <div id="fullscreen-search-wrapper">
        <form method="get" id="fullscreen-searchform">
          <input type="text" value="" placeholder="Type and hit Enter..." id="fullscreen-search-input" class="search-bar-top">
          <i class="fullscreen-search-icon icofont icofont-search">
          <input value="" type="submit">
          </i>
        </form>
      </div>
    </div>
    <!--== End Top Search ==-->
    <div class="container">
      <!--== Start Atribute Navigation ==-->
        <div class="attr-nav no-border hidden-xs">
        <ul class="social-media-dark social-top">
          <li><a href="<?= base_url(); ?>assets/template/#" class="icofont icofont-social-facebook"></a></li>
          <li><a href="<?= base_url(); ?>assets/template/#" class="icofont icofont-social-twitter"></a></li>
          <li><a href="https://www.instagram.com/blackcanyonindonesia/" class="icofont icofont-social-instagram"></a></li>
        </ul>
      </div>
      <!--== End Atribute Navigation ==-->

      <!--== Start Header Navigation ==-->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i class="tr-icon ion-android-menu"></i> </button>
        <div class="logo"> <a href="<?= base_url(); ?>assets/template/index.html"> <img class="logo logo-display" src="<?= base_url(); ?>assets/template/assets/images/logo-white.png" alt=""> <img class="logo logo-scrolled" src="<?= base_url(); ?>assets/template/assets/images/logo-black.png" alt=""> </a> </div>
      </div>
      <!--== End Header Navigation ==-->
 <!--== MENU HEADER==-->
      <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="nav navbar-nav navbar-center" data-in="fadeIn" data-out="fadeOut">  
          <li><a class="page-scroll" href="<?= base_url(); ?>">Home</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>about-us">About Us</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>menu">Menu</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>list-blog">Blog</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>contact">Contact</a></li>
        </ul>
      </div>
      <!--== /.navbar-collapse ==-->
    </div>

  </nav>
  <!--== Header End ==-->

  <!--== Hero Slider Start ==-->
  <div class="remove-padding transition-none" id="home">
  <div id="rev_slider_1078_2_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic4export" data-source="gallery" style="margin:0px auto;background-color:#000000;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
	<div id="rev_slider_1078_2" class="rev_slider fullwidthabanner anno-rs" style="display:none;" data-version="5.4.1">
<ul>	<!-- SLIDE  -->
	<li data-index="rs-3045" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-rotate="0"  data-saveperformance="off"  data-title="" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="<?= base_url(); ?>assets/template/assets/images/three.png"  alt="" class="rev-slidebg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-kenburns="off" data-duration="20000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="200" data-offsetstart="0 0" data-offsetend="0 0" data-rotatestart="0" data-rotateend="0">
		<!-- LAYERS -->

    <!-- LAYER NR. 1 -->
		<div class="hero-text-wrap">
  		<div class="tp-caption NotGeneric-Title tp-resizeme" id="slide-3045-layer-1"
  			data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
  			data-y="['middle','middle','middle','middle']" data-voffset="['-120','-180','-180','-80']"
        data-fontsize="['86','56','56','30']"
  			data-lineheight="['86','76','76','40']"
  			data-width="none"
  			data-height="none"
  			data-whitespace="nowrap"
  			data-type="text"
  			data-responsive_offset="on"
  			data-frames='[{"delay":"+290","speed":2000,"frame":"0","from":"y:50px;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
  			data-textAlign="['center','center','center','center']"
  			data-paddingtop="[10,10,10,10]"
  			data-paddingright="[0,0,0,0]"
  			data-paddingbottom="[10,10,10,10]"
  			data-paddingleft="[0,0,0,0]" style="font-family: 'Montserrat', sans-serif;font-weight:600;">Black Canyon Indonesia </div>

      <!-- LAYER NR. 2 -->
  		<div class="tp-caption NotGeneric-Title tp-resizeme" id="slide-3045-layer-2"
  			data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
  			data-y="['middle','middle','middle','middle']" data-voffset="['60','60','0','0']"
        data-fontsize="['22','22','22','12']"
  			data-lineheight="['38','38','38','20']"
  			data-width="none"
  			data-height="none"
  			data-whitespace="nowrap"
  			data-type="text"
  			data-responsive_offset="on"
  			data-frames='[{"delay":"+490","speed":2000,"frame":"0","from":"y:50px;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
  			data-textAlign="['center','center','center','center']"
  			data-paddingtop="[10,10,10,10]"
  			data-paddingright="[0,0,0,0]"
  			data-paddingbottom="[10,10,10,10]"
  			data-paddingleft="[0,0,0,0]" style="font-family: 'Montserrat', sans-serif;font-weight:600;">A taste from paradise, available on earth</div>
	</li>
	<!-- SLIDE  -->
	<li data-index="rs-3046" data-transition="fadetoleftfadefromright" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-rotate="0"  data-saveperformance="off"  data-title="" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    <!-- MAIN IMAGE -->
		<img src="<?= base_url(); ?>assets/template/assets/images/one.png"  alt="" class="rev-slidebg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-kenburns="off" data-duration="20000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="200" data-offsetstart="0 0" data-offsetend="0 0" data-rotatestart="0" data-rotateend="0">

		<!-- LAYERS -->
    <!-- LAYER NR. 1 -->
    <div class="hero-text-wrap">
      <div class="tp-caption NotGeneric-Title tp-resizeme" id="slide-3045-layer-1"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['middle','middle','middle','middle']" data-voffset="['-120','-180','-180','-80']"
        data-fontsize="['86','56','56','30']"
        data-lineheight="['86','76','76','40']"
        data-width="none"
        data-height="none"
        data-whitespace="nowrap"
        data-type="text"
        data-responsive_offset="on"
        data-frames='[{"delay":"+290","speed":2000,"frame":"0","from":"y:50px;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
        data-textAlign="['center','center','center','center']"
        data-paddingtop="[10,10,10,10]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[10,10,10,10]"
        data-paddingleft="[0,0,0,0]" style="font-family: 'Montserrat', sans-serif;font-weight:600;">Black Canyon Indonesia </div>

      <!-- LAYER NR. 2 -->
      <div class="tp-caption NotGeneric-Title tp-resizeme" id="slide-3045-layer-2"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['middle','middle','middle','middle']" data-voffset="['60','60','0','0']"
        data-fontsize="['22','22','22','12']"
        data-lineheight="['38','38','38','20']"
        data-width="none"
        data-height="none"
        data-whitespace="nowrap"
        data-type="text"
        data-responsive_offset="on"
        data-frames='[{"delay":"+490","speed":2000,"frame":"0","from":"y:50px;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
        data-textAlign="['center','center','center','center']"
        data-paddingtop="[10,10,10,10]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[10,10,10,10]"
        data-paddingleft="[0,0,0,0]" style="font-family: 'Montserrat', sans-serif;font-weight:600;">A taste from paradise, available on earth</div>
  </li>
	<!-- SLIDE  -->
	<li data-index="rs-3047" data-transition="fadefrombottom" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-rotate="0"  data-saveperformance="off"  data-title="" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="<?= base_url(); ?>assets/template/assets/images/two.png"  alt="" class="rev-slidebg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-kenburns="off" data-duration="20000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="200" data-offsetstart="0 0" data-offsetend="0 0" data-rotatestart="0" data-rotateend="0">
		<!-- LAYERS -->
    <!-- LAYER NR. 1 -->
    <div class="hero-text-wrap">
      <div class="tp-caption NotGeneric-Title tp-resizeme" id="slide-3045-layer-1"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['middle','middle','middle','middle']" data-voffset="['-120','-180','-180','-80']"
        data-fontsize="['86','56','56','30']"
        data-lineheight="['86','76','76','40']"
        data-width="none"
        data-height="none"
        data-whitespace="nowrap"
        data-type="text"
        data-responsive_offset="on"
        data-frames='[{"delay":"+290","speed":2000,"frame":"0","from":"y:50px;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
        data-textAlign="['center','center','center','center']"
        data-paddingtop="[10,10,10,10]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[10,10,10,10]"
        data-paddingleft="[0,0,0,0]" style="font-family: 'Montserrat', sans-serif;font-weight:600;">Black Canyon Indonesia </div>

      <!-- LAYER NR. 2 -->
      <div class="tp-caption NotGeneric-Title tp-resizeme" id="slide-3045-layer-2"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['middle','middle','middle','middle']" data-voffset="['60','60','0','0']"
        data-fontsize="['22','22','22','12']"
        data-lineheight="['38','38','38','20']"
        data-width="none"
        data-height="none"
        data-whitespace="nowrap"
        data-type="text"
        data-responsive_offset="on"
        data-frames='[{"delay":"+490","speed":2000,"frame":"0","from":"y:50px;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
        data-textAlign="['center','center','center','center']"
        data-paddingtop="[10,10,10,10]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[10,10,10,10]"
        data-paddingleft="[0,0,0,0]" style="font-family: 'Montserrat', sans-serif;font-weight:600;">A taste from paradise, available on earth</div>
  </li>
	<!-- SLIDE  -->

</ul>
<div class="tp-bannertimer" style="height: 3px; background-color: rgba(255, 255, 255, 0.25);"></div>	</div>
</div>
</div>
  <!--== Hero Slider End ==-->

 
  <!-- ABOUT US -->
    <section style="background:url() right no-repeat #fff;" class="height-650px" style="padding-bottom:0px" >
    <div class="container-fluid">
        <div class="row">
        <div class="col-md-6 col-sm-6 pl-50 pr-70">
            <div class="section-title text-left">
              <h1 style="font: italic 4em Fira Sans serif; color:black;">About Us</h1>
              <p class="mt-30 line-height-20" style="text-align: justify; color: black; font-size: 20px;">
              Fresh air, coffee smell, good cuisine.<br>
              Where else can you get a soothing ambiance, delicious food, and excellent service all in one place? </p>
              <p class="mt-30"><a href="<?= base_url(); ?>c_landing_page/about_us" class="btn btn-lg btn-dark-outline btn-rounded">Find out more about us <i class="icofont icofont-arrow-right" style="font-size: 3em;"></i></a></p>
            </div>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12">
            <img src="<?= base_url(); ?>assets/template/assets/images/bannerbc2.jpg" class="img-responsive" style="border-radius: 0%; width: 100%" />
          
          </div>
        </div>
  </section>
  <!-- END ABOUT US -->

<!--=== About Us Menu menu makanan======-->
<!-- 1 -->
<section class="lg-section" id="about" >
    <div class="container">
      <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
          <img src="<?= base_url(); ?>assets/template/assets/images/gallery/a.png" class="img-responsive"  style="border-radius: 100%; width: 100%; align:left"  />
        
        </div>
        <div class="col-md-7 col-sm-6 col-xs-12 xs-mb-50">
          <div class="section-title text-left">
            <h1 style="font: italic 4em Fira Sans serif;">A Taste  <br>from Paradise</h1>
            <p class="mt-50 font-16px line-height-20"  style="text-align: justify;">We begin by carefully selecting the highest-quality ingredients, which are sourced and delivered directly from reputable and dedicated suppliers. Our skilled and talented chef then cooks these food ingredients, ensuring that the best quality food menu selections are given to customers along with our customer delight experience.</p>
             <p class="mt-30"><a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-lg btn-dark-outline btn-rounded">Read more</a></p>
          </div>
        </div>

        
      </div>
    </div>
  </section>

<!-- 2 -->
  <section class="lg-section" id="about" style="padding-top:0px;" >
    <div class="container">
      <div class="row">
        <div class="col-md-7 col-sm-6 col-xs-12 xs-mb-50">
          <div class="section-title text-left">
            <h1 style="font: italic 4em Fira Sans serif;">Available on    <br>Earth</h1>
            <p class="mt-50 font-16px line-height-20"  style="text-align: justify;">Our goal is to serve specialty coffee. We carefully choose high-quality coffee beans from Indonesia and combine them with beans from other coffee-producing in all over the world. The roasted coffee beans are then brewed by our qualified barista in an espresso machines imported from Italy, which preserves the Black Canyon Coffee's unique taste and standards.</p>
             <p class="mt-30"><a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-lg btn-dark-outline btn-rounded">Read more</a></p>
          </div>
        </div>

        <div class="col-md-5 col-sm-6 col-xs-12">
          <img src="<?= base_url(); ?>assets/template/assets/images/gallery/roti.png" class="img-responsive" style="border-radius: 100%; width: 100%; align:left" />
        
        </div>
      </div>
  </section>
  <!--=== About Us End ======-->


  <!--== artikel/blog  ==-->
 
  <!--== Testimonails Start ==-->
  <section class="white-bg">
     <div class="container">
       <div class="row">
         <div class="col-md-12 text-center">
           <h2 class="mt-0 font-700">Our Blog</h2>
         </div>
       </div>
        <div class="row">
          <div class="testimonial-style-2">
            <div class="col-xs-12">
              <!--== Slide ==-->
              <div class="testimonial-item">
                <div class="testimonial-content">
                  <img class="img-responsive" alt="avatar-1" src="<?= base_url(); ?>/assets/template/assets/images/gallery/kopi.png">
                  <h3 class="font-700 mb-0">Judul Artikel</h3>
                  <h5 class="font-700 mb-0">26 Juni 2021</h5><br> 
                  <p class="line-height-26 font-20px"><span>Very much satisfied with the service. Delivered on time and responded to request for modifications within few hours. I recommend Sloganshub for anyone looking for smart business.</span></p>
                    <p class="mt-30"><a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-lg btn-dark-outline btn-rounded">Read more</a></p>
                </div>
              </div>
            </div>

            <div class="col-xs-12">
              <!--== Slide ==-->
              <div class="testimonial-item text-center">
                <div class="testimonial-content">
                    <img class="img-responsive" alt="avatar-1" src="<?= base_url(); ?>/assets/template/assets/images/gallery/kopi.png">
                  <h3 class="font-700 mb-0">Judul Artikel</h3>
                  <h5 class="font-700 mb-0">26 Juni 2021</h5><br> 
                  <p class="line-height-26 font-20px"><span>Very much satisfied with the service. Delivered on time and responded to request for modifications within few hours. I recommend Sloganshub for anyone looking for smart business.</span></p>
                    <p class="mt-30"><a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-lg btn-dark-outline btn-rounded">Read more</a></p>
                </div>
              </div>
            </div>
            <div class="col-xs-12">
              <!--== Slide ==-->
              <div class="testimonial-item text-center">
                <div class="testimonial-content">
                  <img class="img-responsive" alt="avatar-1" src="<?= base_url(); ?>/assets/template/assets/images/gallery/kopi.png">
                  <h3 class="font-700 mb-0">Judul Artikel</h3>
                  <h5 class="font-700 mb-0">26 Juni 2021</h5><br> 
                  <p class="line-height-26 font-20px"><span>Very much satisfied with the service. Delivered on time and responded to request for modifications within few hours. I recommend Sloganshub for anyone looking for smart business.</span></p>
                    <p class="mt-30"><a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-lg btn-dark-outline btn-rounded">Read more</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>

       
    </div>
  </section>
  <!--== Testimonails Eand ==-->

<!--== Footer Start ==-->
  <footer class="footer">
    <div class="footer-main">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-4">
            <div class="widget widget-text">
              <div class="logo-footer">
                <a href="<?= base_url(); ?>assets/template/index.html"><img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/logo-footer.png" alt=""></a>
              </div>
              <p><i class="icofont icofont-location-pin"></i> Jl. Ahmad Yani, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur</p>
              <p><i class="icofont icofont-phone"></i> 031-99860502</p>
              <p class="mt-20 mb-0"><i class="icofont icofont-email"></i><a href="<?= base_url(); ?>assets/template/#"> indonesiablackcanyon@gmail.com</a></p>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="widget widget-text widget-links">
              <h5 class="widget-title">Location</h5>
              <p>
                  <a href="https://goo.gl/maps/GBBUPBBb7Sq1JtQU7">Surabaya</a><br>
                  <a href="https://goo.gl/maps/P8D2YAqCi8hWKYocA">Makassar</a><br>
                  <a href="https://goo.gl/maps/ijbTSAT83kT9oSjD9">Bali</a><br>      
                  </p>
              
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
              <h5 class="widget-title">Company</h5>
              <ul>
                <li><a href="#">Our Menu</a></li>
                <li><a href="#">Location</a></li>
                <li><a href="#">Contach Us</a></li>
                <li><a href="#">Connect your worlds.</a></li>
              
              </ul>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
              <h5 class="widget-title">Media Sosial</h5>
            <ul class="sm-icon">
                <li><a class="facebook" href="#"><i class="icofont icofont-social-facebook"></i><span></span></a></li>
                <li><a class="twitter" href="#"><i class="icofont icofont-social-twitter"></i><span></span></a></li>
                <li><a class="behance" href="https://www.instagram.com/blackcanyonindonesia//"><i class="icofont icofont-social-instagram"></i><span></span></a></li>
              
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="copy-right text-center">© 2021 Black Canyon Indonesia. All rights reserved</div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--== Footer End ==-->

  <!--== Go to Top  ==-->
  <a href="javascript:" id="return-to-top"><i class="icofont icofont-arrow-up"></i></a>
  <!--== Go to Top End ==-->

</div>
<!--== Wrapper End ==-->


<!--== Javascript Plugins ==-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJNGOwO2hJpJ9kz8e0UUPjZhEbgDJTTXE"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/plugins.js"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/master.js"></script>

<!-- Revolution js Files -->
<script src="<?= base_url(); ?>assets/template/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.actions.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.carousel.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.kenburn.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.layeranimation.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.migration.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.navigation.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.parallax.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.slideanims.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.video.min.js"></script>
<!--== Javascript Plugins End ==-->

</body>
</html>
