<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Black Canyon Cafe</title>
<link rel="shortcut icon" href="<?= base_url(); ?>assets/template/assets/images/favicon.ico">

<!-- Core Style Sheets -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/template/assets/css/master.css">
<!-- Responsive Style Sheets -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/template/assets/css/responsive.css">
<!-- Revolution Style Sheets -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/settings.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/navigation.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<link href='https://fonts.googleapis.com/css?family=Archivo Black' rel='stylesheet'>
<style>

.container {
  position: relative;
  width: 80%;
}
.image {
  display: block;
  width: 100%;
  height: auto;
  -webkit-filter: brightness(20%);
    filter:brightness(40%);
}


.overlay {
  position: absolute;
  bottom: 0;
  left: 100%;
  right: 0;
  background-color: #800008;
  overflow: hidden;
  width: 0;
  height: 100%;
  transition: .5s ease;
}

.container:hover .overlay {
  width: 100%;
  left: 0;
}

.text {
  color: white;
  font-size: 20px;
  position: relative;
  top: 25%;
  left: 25%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  white-space: nowrap;
}
/* Centered text */

.caption {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate( -50%, -50% );
    text-align: center;
    color: white;
    font-weight: bold;
}
</style>
</head>
<body>

<!--== Loader Start ==-->
<div id="loader-overlay">
  <div class="loader">
    <img src="<?= base_url(); ?>assets/template/assets/images/loader.svg" width="80" alt="">
  </div>
</div>
<!--== Loader End ==-->

<!--== Wrapper Start ==-->
<div class="wrapper">

  <!--== Header Start ==-->
  <nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav on no-full no-border">
    <!--== Start Top Search ==-->
    <div class="fullscreen-search-overlay" id="search-overlay"> <a href="<?= base_url(); ?>assets/template/#" class="fullscreen-close" id="fullscreen-close-button"><i class="icofont icofont-close"></i></a>
      <div id="fullscreen-search-wrapper">
        <form method="get" id="fullscreen-searchform">
          <input type="text" value="" placeholder="Type and hit Enter..." id="fullscreen-search-input" class="search-bar-top">
          <i class="fullscreen-search-icon icofont icofont-search">
          <input value="" type="submit">
          </i>
        </form>
      </div>
    </div>
    <!--== End Top Search ==-->
    <div class="container">
      <!--== Start Atribute Navigation ==-->
      <div class="attr-nav no-border hidden-xs">
        <ul class="social-media-dark social-top">
          <li><a href="<?= base_url(); ?>assets/template/#" class="icofont icofont-social-facebook"></a></li>
          <li><a href="<?= base_url(); ?>assets/template/#" class="icofont icofont-social-twitter"></a></li>
          <li><a href="https://www.instagram.com/blackcanyonindonesia/" class="icofont icofont-social-instagram"></a></li>
        </ul>
      </div>
      <!--== End Atribute Navigation ==-->

      <!--== Start Header Navigation ==-->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i class="tr-icon ion-android-menu"></i> </button>
        <div class="logo"> <a href="<?= base_url(); ?>assets/template/index.html"> <img class="logo logo-display" src="<?= base_url(); ?>assets/template/assets/images/logo-white.png" alt=""> <img class="logo logo-scrolled" src="<?= base_url(); ?>assets/template/assets/images/logo-black.png" alt=""> </a> </div>
      </div>
      <!--== End Header Navigation ==-->

      <!--== MENU HEADER==-->
      <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="nav navbar-nav navbar-center" data-in="fadeIn" data-out="fadeOut">  
          <li><a class="page-scroll" href="<?= base_url(); ?>">Home</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>c_landing_page/about_us">About Us</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>c_landing_page/menu">Menu</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>c_landing_page/blog">Blog</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>c_landing_page/kontak">Contact</a></li>
        </ul>
      </div>
      <!--== /.navbar-collapse ==-->
    </div>

  </nav>
  <!--== Header End ==-->


  <!--== Hero Slider Start ==-->
  <section class="remove-padding relative view-height-100vh white-bg" id="home">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 display-table view-height-100vh">
          <div class="v-align-middle text-center hero-text">
            <div class="white-color" >
              <h1 class="font-800" style=" font-family: arial;">Black Canyon Indonesia</h1>
              <h2 class="font-200">A taste from paradise, available on earth</h2>
           
            </div>
          </div>
        </div>
      </div>
    </div>
    <video autoplay="" muted="" loop="" controls="" class="html5-video">
      <source src="<?= base_url(); ?>assets/template/assets/videos/bc.mp4" type="video/mp4">
      <source src="<?= base_url(); ?>assets/template/assets/videos/bc.webm" type="video/webm">
    </video>
  </section>
  <!--== Hero Slider End ==-->

	<!-- ABOUT US -->
		<section style="background:url() right no-repeat #800000;" class="height-650px" style="padding-bottom:0px" >
		<div class="container-fluid">
				<div class="row">
				<div class="col-md-6 col-sm-6 pl-90 pr-70">
						<div class="section-title text-left">
							<h1 style="font: italic 4em Fira Sans serif; color:white;">About Us</h1>
							<p class="mt-50 font-16px line-height-20" style="text-align: justify; color: white;">
								Black Canyon Indonesia offers you a truly fine dining experience with a perfect ambience of a Thailand vibes and a taste of paradise. With both passion and perfection, our experienced chef and barista are only preparing what is best for you -- great food, great coffee, great service and great atmosphere. </p>
							<p class="mt-30"><a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-lg btn-light-outline btn-rounded">Read more</a></p>
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12">
						<img src="<?= base_url(); ?>assets/template/assets/images/bannerbc3.png" class="img-responsive" style="border-radius: 0%; width: 100%" />
					
					</div>
				</div>
	</section>
	<!-- END ABOUT US -->

<!--=== About Us Menu menu makanan======-->
<!-- 1 -->
<section class="lg-section" id="about" >
    <div class="container">
      <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
          <img src="<?= base_url(); ?>assets/template/assets/images/gallery/a.png" class="img-responsive"  style="border-radius: 100%; width: 100%; align:left"  />
        
        </div>
        <div class="col-md-7 col-sm-6 col-xs-12 xs-mb-50">
          <div class="section-title text-left">
            <h1 style="font: italic 4em Fira Sans serif;">A Taste  <br>from Paradise</h1>
            <p class="mt-50 font-16px line-height-20"  style="text-align: justify;">We begin by carefully selecting the highest-quality ingredients, which are sourced and delivered directly from reputable and dedicated suppliers. Our skilled and talented chef then cooks these food ingredients, ensuring that the best quality food menu selections are given to customers along with our customer delight experience.</p>
             <p class="mt-30"><a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-lg btn-dark-outline btn-rounded">Read more</a></p>
          </div>
        </div>

        
      </div>
    </div>
  </section>

<!-- 2 -->
  <section class="lg-section" id="about" style="padding-top:0px;" >
    <div class="container">
      <div class="row">
        <div class="col-md-7 col-sm-6 col-xs-12 xs-mb-50">
          <div class="section-title text-left">
            <h1 style="font: italic 4em Fira Sans serif;">Available on 	  <br>Earth</h1>
            <p class="mt-50 font-16px line-height-20"  style="text-align: justify;">Our goal is to serve specialty coffee. We carefully choose high-quality coffee beans from Indonesia and combine them with beans from other coffee-producing in all over the world. The roasted coffee beans are then brewed by our qualified barista in an espresso machines imported from Italy, which preserves the Black Canyon Coffee's unique taste and standards.</p>
             <p class="mt-30"><a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-lg btn-dark-outline btn-rounded">Read more</a></p>
          </div>
        </div>

        <div class="col-md-5 col-sm-6 col-xs-12">
          <img src="<?= base_url(); ?>assets/template/assets/images/gallery/roti.png" class="img-responsive" style="border-radius: 100%; width: 100%; align:left" />
        
        </div>
      </div>
  </section>
  <!--=== About Us End ======-->


  <!--== artikel/blog  ==-->
  <section class="transition-none" id="blog" style="background:url() center center no-repeat #fff; padding-top: 20px; padding-bottom:20px; ">
  <div class="section-title text-center">
      <h1 class="dark-color">Blog <span class="text-bottom-line-sm"></span></h1>
  </div>
    <div class="container">
      <img src="<?= base_url(); ?>assets/template/assets/images/bcsby2.png" class="image">
      <div class="caption" style="font: bold 3em arial; color: white;">Paradise on Your Plate</div>
      <div class="overlay">
        <p class="caption" style="text-align: center; color: white;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, "<br><br>
        <a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-lg btn-light-outline btn-rounded" >Read more</a></p>
      </div>
    </div>
  </section>


  <!--== Footer Start ==-->
  <footer class="footer">
    <div class="footer-main">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-text">
              <div class="logo-footer">
                <a href="<?= base_url(); ?>assets/template/index.html"><img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/logo-footer.png" alt=""></a>
              </div>
              <p>Jl. Ahmad Yani, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur</p>
              <p class="mt-20 mb-0"><a href="<?= base_url(); ?>assets/template/#">indonesiablackcanyon@gmail.com</a></p>
              <p class="mt-0"><a href="<?= base_url(); ?>assets/template/https://www.google.com/maps/search/Potsdamer+Platz+9797/@52.5096488,13.3737554,17z/data=!3m1!4b1" target="_blank">Black Canyon</a></p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-text widget-links">
              <h5 class="widget-title">Location</h5>
              <p>
                  <a href="https://goo.gl/maps/GBBUPBBb7Sq1JtQU7">Surabaya</a><br>
                  <a href="https://goo.gl/maps/P8D2YAqCi8hWKYocA">Makassar</a><br>
                  <a href="https://goo.gl/maps/ijbTSAT83kT9oSjD9">Bali</a><br>
                
                  </p>
              
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
              <h5 class="widget-title">Company</h5>
              <ul>
                <li><a href="#">Our Menu</a></li>
                <li><a href="#">Location</a></li>
                <li><a href="#">Contach Us</a></li>
                <li><a href="#">Connect your worlds.</a></li>
              
              </ul>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
              <h5 class="widget-title">Media Sosial</h5>
            <ul class="sm-icon">
                <li><a class="facebook" href="#"><i class="icofont icofont-social-facebook"></i><span></span></a></li>
                <li><a class="twitter" href="#"><i class="icofont icofont-social-twitter"></i><span></span></a></li>
                <li><a class="behance" href="https://www.instagram.com/blackcanyonindonesia//"><i class="icofont icofont-social-instagram"></i><span></span></a></li>
              
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="copy-right text-center">© 2021 Black Canyon Indonesia. All rights reserved</div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--== Footer End ==-->

  <!--== Go to Top  ==-->
  <a href="javascript:" id="return-to-top"><i class="icofont icofont-arrow-up"></i></a>
  <!--== Go to Top End ==-->

</div>
<!--== Wrapper End ==-->

<!--== Javascript Plugins ==-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJNGOwO2hJpJ9kz8e0UUPjZhEbgDJTTXE"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/plugins.js"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/master.js"></script>

<!-- Revolution js Files -->
<script src="<?= base_url(); ?>assets/template/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.actions.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.carousel.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.kenburn.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.layeranimation.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.migration.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.navigation.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.parallax.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.slideanims.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.video.min.js"></script>
<!--== Javascript Plugins End ==-->

</body>
</html>
