<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_promote extends CI_Model{

    // Article Home
    public function get_promote_all($where){
    	$this->db->join("article_main am", "am.id_article_main = pa.id_article_pr");
    	$data = $this->db->get_where("pr_article pa", $where);
    	return $data->result();
    }

    // Article Blog
    public function get_promote_all_blog($where){
        // $this->db->join("article_main am", "am.id_article_main = pa.id_article_pr");
        $this->db->join("admin ad", "ad.id_admin = pa.create_admin_article");
        $data = $this->db->get_where("article_main pa", $where);
        return $data->result();
    }


    // Product Home
    public function get_promote_pg_home_all($where){
    	$this->db->join("product_jenis pj", "pph.id_jenis = pj.id_jenis");
        $this->db->order_by("r_num_pr", "ASC");
    	$data = $this->db->get_where("pr_page_home pph", $where);
    	return $data->result();
    }


    // Product Main
    public function get_product_all_like($where, $param1){
        $this->db->join("toko tk", "pr.id_toko = tk.id_toko");
        // $this->db->join("product_brand pb", "pr.id_brand = pb.id_brand");
        $this->db->like('pr.category_produk', $param1);
        $data = $this->db->get_where("product pr", $where);
        return $data->result();
    }

    public function get_product_all_like_perpage($where, $param1, $per_page, $start){
        $this->db->join("toko tk", "pr.id_toko = tk.id_toko");
        // $this->db->join("product_brand pb", "pr.id_brand = pb.id_brand");
        $this->db->like('pr.category_produk', $param1);
        $data = $this->db->get_where("product pr", $where, $per_page, $start);
        return $data->result();
    }

    // Product detail
    public function get_product_detail($where){
        $this->db->join("toko tk", "pr.id_toko = tk.id_toko");
        $data = $this->db->get_where("product pr", $where);
        return $data->row_array();
    }
}
?>