<?php
class C_landing_page extends CI_Controller
{
  function __construct()
  {
      parent::__construct();
  }
   
  function index(){
      $this->load->view('landingpagefoto_view');
  }
   
    function blog(){
      $this->load->view('blog_view');
  }
   function blog_detail(){
      $this->load->view('blog_detail_view');
  }
  function about_us(){
      $this->load->view('about_us_view');
  }
   function menu(){
      $this->load->view('menu_view');
  }
   function kontak(){
      $this->load->view('contact_view');
  }

   function hotcoffee(){
      $this->load->view('menu_all_view');
  }
}