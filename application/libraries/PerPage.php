<?php
class Perpage {
	public $perpage;
	
	function __construct() {
		$this->perpage = 9;
	}

				
                            
                            // <li class="active"><a href="https://blackcayonindonesia.co.id/assets/template/#">1</a></li>
                            // <li><a href="https://blackcayonindonesia.co.id/assets/template/#">2</a></li>
                            // <li><a href="https://blackcayonindonesia.co.id/assets/template/#">3</a></li>
                            // <li><a href="https://blackcayonindonesia.co.id/assets/template/#">Next <i class="icofont icofont-long-arrow-right ml-5 xs-display-none"></i></a></li>
                        
	
	function getAllPageLinks($count, $href, $per_page) {
		$this->perpage = $per_page;
		$output = '	<div class="text-center">
						<div class="pagination dark-color">
							<ul>';
		if(!isset($_GET["page"])) $_GET["page"] = 1;
		if($this->perpage != 0)
			$pages  = ceil($count/$this->perpage);
		if($pages>1) {
			if($_GET["page"] == 1) 
				$output = $output . '<li><a href="#"><i class="icofont icofont-long-arrow-left mr-5 xs-display-none"></i> Prev</a></li>';
			else	
				$output = $output . '<li><a class="link" onclick="set_link(\'' . $href . ($_GET["page"]-1) . '&per_page=' .$per_page. '\')" >Prev</a></li>';
			
			
			if(($_GET["page"]-3)>0) {
				if($_GET["page"] == 1)
					$output = $output . '<li><a id=1 class="link current">1</a></li>';
				else				
					$output = $output . '<li><a class="link" onclick="set_link(\'' . $href . '1'. '&per_page=' .$per_page. '\')" >1</a></li>';
			}
			if(($_GET["page"]-3)>1) {
					$output = $output . '<li><a class="dot">...</a></li>';
			}
			
			for($i=($_GET["page"]-2); $i<=($_GET["page"]+2); $i++)	{
				if($i<1) continue;
				if($i>$pages) break;
				if($_GET["page"] == $i)
					$output = $output . '<li><a id='.$i.' class="link current">'.$i.'</a></li>';
				else				
					$output = $output . '<li><a class="link" onclick="set_link(\'' . $href . ($i) . '&per_page=' .$per_page.  '\')" >'.$i.'</a></li>';
			}
			
			if(($pages-($_GET["page"]+2))>1) {
				$output = $output . '<li><a class="dot">...</a></li>';
			}

			if(($pages-($_GET["page"]+2))>0) {
				if($_GET["page"] == $pages)
					$output = $output . '<li><a id=' . ($pages) .' class="link current">' . ($pages) .'</a></li>';
				else				
					$output = $output . '<li><a class="link" onclick="set_link(\'' . $href .  ($pages). '&per_page=' .$per_page .'\')" >' . ($pages) .'</a></li>';
			}
			
			if($_GET["page"] < $pages)
				$output = $output . '<li><a class="link" onclick="set_link(\'' . $href . ($_GET["page"]+1) .'&per_page=' .$per_page. '\')" >Next<i class="icofont icofont-long-arrow-right ml-5 xs-display-none"></i></a></li>';
			else				
				$output = $output . '<li><a class="link disabled">Next</a></li>';
			
			
		}

		$output .= '	</ul>
					</div>
				</div>';

		return $output;
	}
	function getPrevNext($count,$href) {
		$output = '';
		if(!isset($_GET["page"])) $_GET["page"] = 1;
		if($this->perpage != 0)
			$pages  = ceil($count/$this->perpage);
		if($pages>1) {
			if($_GET["page"] == 1) 
				$output = $output . '<span class="link disabled first">Prev</span>';
			else	
				$output = $output . '<a class="link first" onclick="set_link(\'' . $href . ($_GET["page"]-1) . '\')" >Prev</a>';			
			
			if($_GET["page"] < $pages)
				$output = $output . '<a  class="link" onclick="set_link(\'' . $href . ($_GET["page"]+1) . '\')" >Next</a>';
			else				
				$output = $output . '<span class="link disabled">Next</span>';
			
			
		}
		return $output;
	}
}
?>