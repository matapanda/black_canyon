 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Magic_pattern {

    public $TAG = "";

    public function __construct(){
        $this->TAG = get_class($this);
    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function set_list_pattern($arr_list = []){
        $status = false;

        if($arr_list){
            $arr_of_status = [];
            foreach ($arr_list as $key => $value) {
                $return_check = $this->main_function($value[0], $value[1]);
                array_push($arr_of_status, $return_check);
                
                if($return_check){
                    break;
                }
            }
        }
        
        if(in_array(true, $arr_of_status)){
            $status = true;
        }

        return $status;
    }

    public function main_function($main_function = "", $str = ""){
        $status = false;
        if($main_function != ""){
            $status = preg_match($this->$main_function(), $str);
        }
        
        return $status;
    }

    private function detected_symbol(){
    	$pattern = "/([^'|\"|;|&|@|#|`|#|\\$|<|>|\||\\\|\/|\n|=])/";
        return $pattern;
    }

    private function allowed_char(){
    	$pattern = "/[0-9|a-z]*/";
        return $pattern;
    }

    private function sample1(){
    	$pattern = "/([^a-zA-Z0-9-&;,.-_@()\s\n]|[\\$|<|>|\||\\\|\/|=])|(false)/";
    	return $pattern;
    }

    private function sample2(){
    	$pattern = "/['\";&@#`#\\$<>\|(\\\)(\/)(\n)(=)]/";
    	return $pattern;
    }

    public function allowed_general_char(){
    	// $pattern = "/[^a-zA-Z0-9-&;,.-_@()\s\n .+]/";
        $pattern = "/[^a-zA-Z0-9-&;,.-_@()\s\n]|[\\$|<|>|\||\\\|\/|=]/";
    	return $pattern;
    }

    private function allowed_general_char_with_enter(){
        $pattern = "/[^a-zA-Z0-9-&;,.-_@()\s\n]|[\\$|<|>|\||\\\|\/|=]/";
        return $pattern;
    }

    public function removing_script(){
        $pattern = "/<.+>/";
        return $pattern;
    }
}